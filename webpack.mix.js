let mix = require("laravel-mix");
mix.disableNotifications();
mix.setPublicPath("public");

mix
  .js("src/js/app.js", "public/js")
  .postCss("src/css/app.css", "public/css", [
    require("postcss-import"),
    require("tailwindcss"),
  ]);

﻿## **Test I4Digital - Angular 1**
A continuación puede encontrar una guía para correr el proyecto designado para la prueba técnica de Angular 1.
Como requisito previo utilizaremos las siguientes tecnologías:

 1. **Node.js**: entorno en tiempo de ejecución multiplataforma. Si aún no tiene configurado en su maquina este entorno, puede seguir la documentación en el siguiente enlace. https://nodejs.org/es/
 2. **NPM**: el sistema de gestión de paquetes por defecto para Node.js, un entorno de ejecución para JavaScript. Por lo general viene configurado con la instalación anterior.
 3. **GIT**: sistema de control de versiones. Si aún no tiene configurado en su maquina este entorno, puede seguir la documentación en el siguiente enlace. https://git-scm.com/

Para la ejecución del proyecto seguiremos los siguientes pasos:

1. Clonar el repositorio en su maquina, para lograr esto ejecutamos el comando `git clone <repo>` donde remplazaremos <**repo**> por la url que nos proporciona GitLab para clonar el proyecto.
2. Ingresar a la carpeta raíz del proyecto desde la consola y ejecutar el comando `npm install` para obtener las dependencias del desarrollo.
3. Ejecutar el comando de compilación `npm run prod` para empaquetar el proyecto para producción *(minify-build-optimize).*
4. Ejecutar el comando `npm start` para inicializar el proyecto y acceder a la url que nos brinda el resultado de esta ejecución.

*Eso es todo, ahora se debe estar corriendo el proyecto para la prueba de Angular 1.*

Para cualquier tipo de inquietud, comunicarse a los siguientes datos:

 - Celular: 3116516076
 - Correo: juanpablomadariagacardona@gmail.com

Gracias por la oportunidad, es un placer haber hecho parte de este proceso.

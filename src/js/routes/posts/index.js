import template from './index.html';
export default {
    title: 'Posts',
    template: template,
    controller: 'PostController'
}
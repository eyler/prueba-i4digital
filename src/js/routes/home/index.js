import template from './index.html';
export default {
    title: 'Home',
    template: template,
    controller: 'HomeController'
}
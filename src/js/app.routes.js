import home from "./routes/home/index";
import users from "./routes/users/index";
import albums from "./routes/albums/index";
import posts from "./routes/posts/index";

export default [
  "$routeProvider",
  "$locationProvider",
  function ($routeProvider, $locationProvider) {
    $routeProvider
      .when("/home", home)
      .when("/users", users)
      .when("/albums", albums)
      .when("/posts", posts)
      .otherwise({
        redirectTo: "/home",
      });
    $locationProvider.hashPrefix("");
  },
];

import template from './navbar.html';

let navbar = {
    template: template,
    transclude: true,
    controller:  ['$scope',function ($scope) {
        $scope.showMenu = false;
        $scope.toggleMenu =()=>{            
            $scope.showMenu = !$scope.showMenu;
        }   
    }],    
};
export default navbar;


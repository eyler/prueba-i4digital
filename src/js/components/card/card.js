import template from "./card.html";

let input = {
  template: template,
  controller: function () {
    var self = this;
    self.album = typeof self.album != "undefined" ? self.album : null;
  },
  bindings: {
    src: "=",
    title: "=",
    id: "=",
    album: "=?",
  },
};
export default input;

export default [
  "$scope",
  "$http",
  "$timeout",
  "endPoint",
  ($scope, $http, $timeout, endPoint) => {
    $scope.data = [];
    $scope.users = [];
    $scope.page = 1;
    $scope.total = 0;
    $scope.search = "";
    $scope.type = "";

    $http.get(`${endPoint.url}/users`).then(function (response) {
      $scope.data = response.data;
      var jobs = ["Gerente", "Contratista", "Planta"];
      $scope.data.map((user) => {
        Object.assign(user, {
          type: jobs[Math.floor(Math.random() * jobs.length)],
        });
      });

      $scope.total = $scope.users.length;
      $scope.getUsers(1);
    });

    $scope.getUsers = (page) => {
      $scope.users = $scope.data;
      if ($scope.search != "") {
        $scope.users = $scope.data.filter((user) => {
          return (
            user.name.toLowerCase().includes($scope.search.toLowerCase()) ||
            user.email.toLowerCase().includes($scope.search.toLowerCase()) ||
            user.address.street.toLowerCase().includes($scope.search.toLowerCase()) ||
            user.address.city.toLowerCase().includes($scope.search.toLowerCase()) ||
            user.company.name.toLowerCase().includes($scope.search.toLowerCase()) ||
            user.username.toLowerCase().includes($scope.search.toLowerCase())
          );
        });
      }

      if ($scope.type != "") {
        $scope.users = $scope.users.filter((user) => {
          return user.type.toLowerCase() == $scope.type.toLowerCase();
        });
      }

      $scope.total = $scope.users.length;
      $scope.page = page;
      $scope.users = $scope.users.slice((page - 1) * 2, page * 2);
    };

    $scope.$watch("search", () => {
      $scope.getUsers(1);
    });

    $scope.$watch("type", () => {
      $scope.getUsers(1);
    });
  },
];

export default [
  "$scope",
  "$http",
  "endPoint",
  ($scope, $http, endPoint) => {
    $scope.getRandomInt = (min, max) => {
      return Math.floor(Math.random() * (max - min)) + min;
    };    
    $scope.photos = [];
    $http
      .get(
        `${endPoint.url}/photos?_start=${$scope.getRandomInt(0, 5000)}&_limit=4`
      )
      .then(function (response) {        
        $scope.photos = response.data;        
      });      
  },
];

export default [
  "$scope",
  "$http",
  "endPoint",
  ($scope, $http, endPoint) => {
    $scope.getRandomInt = (min, max) => {
      return Math.floor(Math.random() * (max - min)) + min;
    };
    $scope.photos = [];
    $http
      .get(
        `${endPoint.url}/albums?_start=${$scope.getRandomInt(0, 10)}&_limit=8`
      )
      .then(function (response) {
        $scope.albums = response.data;
        $scope.albums.map((album) => {
          Object.assign(album, {
            url: `https://picsum.photos/350/220?random=${$scope.getRandomInt(0,1000000)}`,
          });
        });
      });
  },
];

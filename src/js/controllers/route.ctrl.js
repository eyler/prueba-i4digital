export default [
  "$scope",
  "$location",
  "$rootScope",
  "$document",
  function ($scope, $location, $rootScope, $document) {
    $rootScope.$on("$routeChangeStart", function () {
      var route = $location.path();
      $scope.currentRoute = route.substring(1, route.lenght);
      
      $document.ready(function () {
        setTimeout(() => {
          var wrapper = document.getElementById("wrapper");
          var router = document.getElementById("routedContent");
          wrapper.style.minHeight = router.clientHeight + 80 + "px";
        }, 255);
      });
    });
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {      
      $document[0].title =current.$$route.title;
      $rootScope.title = current.$$route.title;
  });
  },
];

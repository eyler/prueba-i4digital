export default [
  "$scope",
  "$http",
  "endPoint",
  "$timeout",
  ($scope, $http, endPoint, $timeout) => {
    $scope.create = false;
    $scope.posts = [];
    $scope.newPost = {
      title: "",
      body: "",
      userId: 1,
    };

    $scope.getPosts = () => {
      $http.get(`${endPoint.url}/posts?userId=1`).then(function (response) {
        $scope.posts = response.data;
        var tempPosts = JSON.parse(localStorage.getItem('posts')||'[]');
        $scope.posts = $scope.posts.concat(tempPosts);
        $scope.posts = $scope.posts.filter((item, pos) => $scope.posts.indexOf(item) === pos)        
      });
    };
    $scope.getPosts();

    $scope.storePost = function() {
      $http
        .post(`${endPoint.url}/posts`, JSON.stringify($scope.newPost), {
          headers: {
            "Content-type": "application/json; charset=UTF-8",
          },
        })
        .then((response) => {
          $scope.posts.push(response.data)
          var tempPosts = JSON.parse(localStorage.getItem('posts')||'[]');
          tempPosts.push(response.data);
          localStorage.setItem('posts',JSON.stringify(tempPosts));
          $scope.newPost.title = "";
          $scope.newPost.body = "";
          $scope.storePostForm.$setPristine();
          $scope.storePostForm.$setUntouched();
          $scope.message = "Post creado exitosamente";
          $timeout(() => {
            $scope.message = "";
          }, 4000);
        });
    };

    $scope.$watch("create", (newVal) => {
      if (!newVal) {
        $scope.getPosts();
      }    
    });
  },
];

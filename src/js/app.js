import angular from "angular";
//componentes
import card from "./components/card/card";
import navbar from "./components/navbar/navbar";

//rutas
import routes from './app.routes';

//controladores
import RouteController from "./controllers/route.ctrl";
import HomeController from "./controllers/home.ctrl";
import UserController from "./controllers/user.ctrl";
import AlbumController from "./controllers/album.ctrl";
import PostController from "./controllers/post.ctrl";

let app = angular.module("app", [
  require("angular-route"),
  require("angular-animate"),
  require('angular-messages')
]).constant('endPoint',{
  url: 'https://jsonplaceholder.typicode.com'
});

app.component("navbar", navbar).component("card", card);

app.config(routes);

app
  .controller("HomeController", HomeController)
  .controller("UserController", UserController)
  .controller("AlbumController", AlbumController)
  .controller("PostController", PostController)
  .controller("RouteController", RouteController);

// export default app;
